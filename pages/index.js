import dynamic from 'next/dynamic'
import Head from 'next/head'
import useSWR from 'swr'

import { fetcher } from '../services/api'
import { enhanceData } from '../services/data-tools'

const Map = dynamic(
    () => import('../components/Map'),
    { ssr: false }
)

const enhancement = (feature) => {
    if (feature.properties.ID_2 % 5 == 0) { feature.properties.style = "circle" }
    return feature
}

const HomePage = () => {
    const {data, error} = useSWR('/district/india_district.geojson', fetcher)

    if (error) return <div>Failed to load districts</div>
    if (!data) return <div>Loading districts...</div>
    const enhancedData = enhanceData(data, enhancement)
    return <>
        <Head>
            <link rel="stylesheet" href="https://unpkg.com/leaflet@1.6.0/dist/leaflet.css"
                integrity="sha512-xwE/Az9zrjBIphAcBb3F6JVqxf46+CDLwfLMHloNu6KEQCAWi6HcDUbeOfBIptF7tcCzusKFjFw2yuvEpDL9wQ=="
                crossorigin=""/>
            <link rel="stylesheet" href="/styles.css" />
        </Head>
        <Map style={{
            height: `200px`
        }} data={enhancedData}/>
    </>
}

export default HomePage
