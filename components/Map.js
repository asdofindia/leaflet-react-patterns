import * as RL from 'react-leaflet'
import { GeoJSONFillable, Patterns } from 'react-leaflet-geojson-patterns'

const { Map: RLMap, TileLayer, GeoJSON } = RL

const getColor = (id) => {
    return id > 14 ? '#f00266' :
        id > 7  ? '#a002aa' :
        id > 3 ? '#600266' :
        '#000200'
}

const getStyle = (feature, layer) => {
    return {
        color: getColor(feature.properties.ID_1),
        weight: 1,
        fillOpacity: 1,
        fillPattern: getPattern(feature)
    }
}
const getStyleNoPattern = (feature, layer) => {
    return {
        color: getColor(feature.properties.ID_1),
        weight: 1,
        fillOpacity: 1
    }
}

const getPattern = feature => {
    switch (feature.properties.style) {
        case 'circle': return Patterns.CirclePattern({
            key: 'circle',
            x: 7,
            y: 7,
            radius: 1,
            height: 15,
            width: 15,
            fill: true,
            opacity: 1,
            color: getColor(feature.properties.ID_1),
            fillColor: getColor(feature.properties.ID_1)
        })
        default: return null;
    }
}

const Map = ({data}) => {
    const position = [22.5, 83.5]
    const zoom = 5

    return (
        <RLMap center={position} zoom={zoom}>
            <TileLayer
                 attribution='&amp;copy <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                 url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                 />
            <GeoJSON data={data} style={getStyleNoPattern}/>
            <GeoJSONFillable data={data} style={getStyle}/>
        </RLMap>

    )
}

export default Map
