const mergeProperty = (key, value, feature) => {
    feature.properties.key = value
    return feature
}

const enhanceData = (data, enhancements) => {
    data.features = data.features.map(enhancements)
    return data
}

export {
    enhanceData
}
