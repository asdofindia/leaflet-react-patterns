import fetch from 'unfetch'

const API_URL = 'http://map.localhost/api'

const fetcher = async (path) => {
    const res = await fetch(API_URL + path)
    const json = await res.json()
    return json
}

export {
    fetcher
}
